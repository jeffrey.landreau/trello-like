// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDiux9RNNebQRloTHk4lhS1baxrNXpVsjY",
    authDomain: "trello-like-716d3.firebaseapp.com",
    projectId: "trello-like-716d3",
    storageBucket: "trello-like-716d3.appspot.com",
    messagingSenderId: "4385639054",
    appId: "1:4385639054:web:0ae046e2019d41c4f997df",
    databaseURL: "https://trello-like-716d3-default-rtdb.europe-west1.firebasedatabase.app",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);