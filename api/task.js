import { uuidv4 } from "@firebase/util";
import { child, get, getDatabase, onValue, ref, set } from "firebase/database";
import { app } from "./app";

const database = getDatabase(app);

export function getAllTasks(uid) {
    return new Promise((resolve, reject) => {
        try {
            const reference = ref(database, 'tasks/' + uid);
            console.log(reference);
            onValue(reference, (snapshot) => {
                const data = snapshot.val();
                resolve(data)
            });
        }
        catch (e) {
            reject(e)
        }
    })
}

export function createTask(uid, taskName) {
    return new Promise((resolve, reject) => {
        try {
            const reference = ref(database);
            get(child(reference, `tasks/${uid}`)).then((snapshot) => {
                const data = snapshot.val() ?? [];
                data.push({ id: uuidv4(), name: taskName })
                console.log(data);
                set(ref(database, 'tasks/' + uid), data);
                resolve(data)
            }).catch(err => {
                console.log(err);
            });
        }
        catch (e) {
            reject(e)
        }
    })
}